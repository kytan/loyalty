@extends('layouts.master')

@section('content')

<h1><div class="panel-heading" ><strong>{{ $thread->name }}</strong></div></h1>

<div class="row">
        @foreach( $replies as $reply )
        <div class="panel-body" >
            <hr>
            {{ $reply->body }}
            ( <strong>City</strong>={{ $reply->city }}, <strong>Latitude</strong>={{ $reply->latitude }},<strong>Longtitud</strong>e={{ $reply->longtitude }},<strong>Temperature</strong>={{ $reply->temperature }} )
        </div>
        @endforeach
</div>
<br><br>
<form method="post" action="{{ $thread->path().'/replies' }}" >
    {{ csrf_field() }}

    <div class="form-group" >
        <label>City</label>
        {!! Form::text('city' , null , array('class' => 'form-control', 'id'=>'city') ) !!}

    </div>

    <div class="form-group" >
        <label>Latitude</label>
        {!! Form::text('latitude' , null , array('class' => 'form-control', 'id'=>'latitude') ) !!}

    </div>

    <div class="form-group" >
        <label>Longtitude</label>
        {!! Form::text('longtitude' , null , array('class' => 'form-control', 'id'=>'longtitude') ) !!}

    </div>

    <div class="form-group" >
        <label>Temperature</label>
        {!! Form::text('temperature' , null , array('class' => 'form-control', 'id'=>'temperature') ) !!}

    </div>

    <div class="form-group" >
        <label >Reply</label><br>
        {!! Form::textarea('body' , null , array('class' => 'form-control', 'id'=>'body','placeholder'=>"Comments?", 'rows'=>3,'cols'=>50) ) !!}
    <br>
    <button type="submit" >Post</button>

</div>
</form>

@endsection
