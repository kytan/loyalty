@extends('layouts.master')

@section('content')
    <h1>Public Posts</h1>
        <form >
        Title : <input type="text" id="txtName" name="txtName" value="">
        <button id="btnDone"  >Done</button>

		<br><br>

            <table id="divResponse" width="100%" cellspacing="1" border="1">
                <thead><tr><td><strong>Username</strong></td></tr></thead>
                @foreach( $alldata as $data )
                    <tr><td><strong><a href="{{ $data->path() }}" >{{ $data->name }}</a></strong></td></tr>
                @endforeach

            </table>
<!--
        <div class="row" >
            <div class="col-md-8 col-lg-offset-2 ">
                @foreach( $alldata as $data )
                    <div class="panel-heading" ><a href="{{ $data->path() }}" >{{ $data->name }}</a></div>
                @endforeach
            </div>

        </div>
-->
        </form>
    </div>


<script>
    $(document).ready( function () {
        $("#btnDone").click( function( e ) {

            e.preventDefault();
            var name = $('#txtName').val() ;

            $.ajax({
                url: '/api/mydata/'+$('#txtName').val() ,
                type: 'GET',

                success: function (data) {

                    $('#divResponse').append('<tr><td>'+data.name+'</td></tr>');

                },
                error: function(xhr) {
                    alert('Please enter a value for Name');
                }
            });

        });

    });

</script>

@endsection