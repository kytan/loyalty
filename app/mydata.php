<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mydata extends Model
{
    //
    protected $table = 'mydata';
    protected $fillable = array('name');

    protected $guarded = [];

    public function replies()
    {
        $this->hasMany( Reply::class );
    }

    public function path()
    {
        return '/thread/' . $this->id;
    }

}
