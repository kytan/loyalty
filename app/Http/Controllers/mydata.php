<?php

namespace App\Http\Controllers;

use App\Providers\AppServiceProvider;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class mydata extends Controller
{
    //
    public function show( $data )
    {
        if ( trim( $data) == "" )
            return response()->json(['error' => 'data empty'] , 404 );
        else {

            $mydata = new \App\mydata();//add data to db
            $mydata->name = $data;
            $mydata->save();

            return response()->json(['name' => $data]);
        }
    }

    /**
     * show home page and all data
     * @return $this
     */
    public function index()
    {
        $alldata = \App\mydata::all();

        return view('home')->with('alldata', $alldata);
    }


	public function done( $data )
	{
		return $data;
	}

	public function showThread( \App\mydata $data, $id )
    {
        //get thread
        $thread = \App\mydata::where('id', '=', $id )->first();
        //get all replies belong to thread
        $replies = \App\Reply::where('mydata_id', '=', $thread->id )->get();

        return view('thread.show')->with('thread', $thread)
                                ->with('replies', $replies );
    }
	
}