<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReplyController extends Controller
{
    //

    /**
     * save reply to db
     *
     * @param \App\mydata $data
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store( \App\mydata $data, $id )
    {
        $reply = new \App\Reply();
        $reply->mydata_id = $id;
        $reply->body = request('body');
        $reply->city = request('city');
        $reply->latitude = request('latitude');
        $reply->longtitude = request('longtitude');
        $reply->temperature = request('temperature');
        $reply->save();

        return back();

    }

}
