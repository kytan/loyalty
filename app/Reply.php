<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    //

    protected $guarded = [];

    public function owner()
    {
        return $this->belongsTo(mydata::class , 'mydata_id' );
    }

}
