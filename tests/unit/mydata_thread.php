<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    use DatabaseMigrations;

    protected $thread;

    public function setUp()
    {

        parent::setUp();

        $this->thread = Factory('App\mydata')->create();

    }


    public function testUserCanBrowseThread()
    {

        $response = $this->get('/');

        $response->assertResponseStatus( 200   );

    }

    public function testUserCanReadAThread()
    {
        $response = $this->get('/thread/' . $this->thread->id );

        $response->assertResponseStatus( 200  );
    }



}
