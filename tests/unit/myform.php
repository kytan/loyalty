<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testMyPageHasComponents()
    {
        $this->visit('/')
            ->see('txtname')
            ->see('btnDone')
            ->see('divResponse');
    }


}