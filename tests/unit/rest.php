<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class RestTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testRestData()
    {
        //returns the text that is passed to it
         $this->get('/api/mydata/john')
            ->seeJson([
                'name' => 'john'
            ]);

    }

	public function testRestNoParam() 
	{
        //if no parameter pass in, throw error
        $this->get('/api/mydata/')
            ->assertResponseStatus(404);
		
	}

}