<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'mydata@index');

Route::get('/done/{data}', 'mydata@done');

Route::get('/thread/{id}' , 'mydata@showThread');

Route::post('/thread/{id}/replies', 'ReplyController@store');